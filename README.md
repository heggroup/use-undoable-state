# UseUndoableState

This is a simple hook for wrapping state in standard undo / redo behaviour.

Published on NPM [Here](https://www.npmjs.com/package/use-undoable-state)

## Installation

To install the package run either:

```bash
yarn add use-undoable-state
```

or

```bash
npm i use-undoable-state
```

## Getting Started

Here is an example of how to use the hook:

```typescript
const [ state, setState, undoState, redoState ] = useUndo()
```

After this it can be used like standard react state.