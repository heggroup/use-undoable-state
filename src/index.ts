import { useState } from "react";

type UndoReturn<U> = [U, (value: U) => void, () => void, () => void];

type UndoState<V> = {
  state: V;
  stack: V[];
  pointer: number;
};

export const useUndo = function useUndo<T>(initialValue: T): UndoReturn<T> {
  const [{ state, stack, pointer }, setState] = useState<UndoState<T>>({
    state: initialValue,
    stack: [initialValue],
    pointer: 0,
  });

  const setStateAndRemember = (value: T) => {
    if (pointer !== 0) {
      for (var i = 0; i < pointer; i++) {
        stack.pop();
      }
    }
    setState({
      state: value,
      stack: [...stack, value],
      pointer: 0,
    });
  };

  const undo = () => {
    if (pointer < stack.length) {
      setState({
        state: stack[stack.length - (pointer + 2)],
        stack,
        pointer: pointer + 1,
      });
    }
  };

  const redo = () => {
    setState({
      state: stack[stack.length - pointer],
      stack,
      pointer: pointer - 1,
    });
  };

  return [state, setStateAndRemember, undo, redo];
};

module.exports = { useUndo };
