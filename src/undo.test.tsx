/**
 * @jest-environment jsdom
 */

import { useUndo } from "./index";
import React from "react";
import { act, fireEvent, render } from "@testing-library/react";

const TestComponent = (props: { initialValue: string }) => {
  const [string, setString, undoString, redoString] = useUndo(
    props.initialValue
  );
  return (
    <>
      <p>VALUE={string}</p>
      <input
        type="text"
        data-testid="input"
        id="input"
        onChange={() =>
          setString(
            (document.getElementById("input") as HTMLInputElement).value
          )
        }
      />
      <button onClick={undoString}>UNDO</button>
      <button onClick={redoString}>REDO</button>
    </>
  );
};

describe("ReactUndo", () => {
  test("Initializes state with given value", () => {
    const screen = render(<TestComponent initialValue="test" />);
    expect(screen.getByText("VALUE=test")).toBeDefined();
  });

  test("State can be updated", () => {
    const screen = render(<TestComponent initialValue="test" />);

    const input = screen.getByTestId("input");
    act(() => {
      fireEvent.change(input, { target: { value: "beans" } });
    });

    expect(screen.getByText("VALUE=beans")).toBeDefined();
  });

  test("State can be updated and undone", async () => {
    const screen = render(<TestComponent initialValue="test" />);

    const input = screen.getByTestId("input");
    act(() => {
      fireEvent.change(input, { target: { value: "beans" } });
    });

    expect(screen.getByText("VALUE=beans")).toBeDefined();

    act(() => {
      screen.getByText("UNDO").click();
    });

    expect(await screen.findByText("VALUE=test")).toBeDefined();
  });

  test("State can be updated and undone and redone", async () => {
    const screen = render(<TestComponent initialValue="test" />);

    const input = screen.getByTestId("input");
    act(() => {
      fireEvent.change(input, { target: { value: "beans" } });
    });

    expect(screen.getByText("VALUE=beans")).toBeDefined();

    act(() => {
      screen.getByText("UNDO").click();
    });

    expect(await screen.findByText("VALUE=test")).toBeDefined();

    act(() => {
      screen.getByText("REDO").click();
    });

    expect(await screen.findByText("VALUE=beans")).toBeDefined();
  });

  test("State can be updated, undone, edited and then cannot be redone", async () => {
    const screen = render(<TestComponent initialValue="test" />);

    const input = screen.getByTestId("input");
    act(() => {
      fireEvent.change(input, { target: { value: "beans" } });
    });

    expect(screen.getByText("VALUE=beans")).toBeDefined();

    act(() => {
      screen.getByText("UNDO").click();
    });

    expect(await screen.findByText("VALUE=test")).toBeDefined();

    act(() => {
      fireEvent.change(input, { target: { value: "toast" } });
    });

    screen.getByText("REDO").click();

    expect(await screen.findByText("VALUE=toast")).toBeDefined();
  });

  test("State can be updated, undone, updated again and then undone", async () => {
    const screen = render(<TestComponent initialValue="test" />);

    const input = screen.getByTestId("input");
    act(() => {
      fireEvent.change(input, { target: { value: "beans" } });
    });

    expect(screen.getByText("VALUE=beans")).toBeDefined();

    act(() => {
      screen.getByText("UNDO").click();
    });

    expect(await screen.findByText("VALUE=test")).toBeDefined();

    act(() => {
      fireEvent.change(input, { target: { value: "toast" } });
    });

    act(() => {
      screen.getByText("UNDO").click();
    });

    expect(await screen.findByText("VALUE=test")).toBeDefined();
  });

  test("Undo without any changes is a noop", async () => {
    const screen = render(<TestComponent initialValue="test" />);
    expect(screen.getByText("VALUE=test")).toBeDefined();

    screen.getByText("UNDO").click();

    expect(await screen.findByText("VALUE=test")).toBeDefined();
  });

  test("Redo without any undos is a noop", async () => {
    const screen = render(<TestComponent initialValue="test" />);

    expect(screen.getByText("VALUE=test")).toBeDefined();

    const input = screen.getByTestId("input");
    fireEvent.change(input, { target: { value: "beans" } });

    screen.getByText("REDO").click();

    expect(await screen.findByText("VALUE=beans")).toBeDefined();
  });
});
